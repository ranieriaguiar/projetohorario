O programa recebe um determinado horário (hora/minutos) e retorna o menor ângulo entre os 2 ponteiros do relógio.

O programa foi desenvolvido com Java 1.8;

Compilado com o Maven;

Foi utilizado o Apache Tomcat 9 como container de servlets;

O framework Jersey foi usado;

Comunicação por REST e com retorno em JSON através do endereço:

http://localhost:8080/desafioHorario/rest/clock/9/0

e com retorno:

{"angle":"90","hour":"9","minute":"0"}