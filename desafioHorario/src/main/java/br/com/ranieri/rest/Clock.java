package br.com.ranieri.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Clock {

	public Clock(int hour, int minute) {
		this.hour = hour;
		this.minute = minute;
	}

	public Clock() {
	}

	private int hour;

	private int minute;

	private int angle;

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getAngle() {
		return angle;
	}

	public void setAngle(int angle) {
		this.angle = angle;
	}

}
