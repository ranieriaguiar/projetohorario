package br.com.ranieri.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.NotFoundException;

@Path("/clock")
public class RESTfulClock {

	private ClockController controller = new ClockController();
	private Clock clock;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{hour}/{minute}")
	public Clock getAngleHourMinute(@PathParam("hour") int hour, @PathParam("minute") int minute) {
		try {
			if (hour > 24 || minute > 59) {
				throw new NotFoundException("Hora inválida!");
			} else {
				clock = new Clock(hour, minute);
			}
			if (controller.searchAngle(clock) >= 0) {
				return clock;
			}

			int angleHour = controller.calculateHourAngle(clock);
			int angleMinute = controller.calculateMinuteAngle(clock);
			clock.setAngle(controller.compareAngle(angleHour, angleMinute));
			controller.addAngle(clock);
			return clock;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("{hour}")
	public Clock getAngleHour(@PathParam("hour") int hour) {
		try {
			if (hour > 24) {
				throw new NotFoundException("Hora inválida!");
			} else {
				clock = new Clock(hour, 0);
			}
			if (controller.searchAngle(clock) >= 0) {
				return clock;
			}

			int angleHour = controller.calculateHourAngle(clock);
			clock.setAngle(controller.compareAngle(angleHour, 0));
			controller.addAngle(clock);
			return clock;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
