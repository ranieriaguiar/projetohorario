package br.com.ranieri.rest;

import java.util.ArrayList;
import java.util.List;

public class ClockController {

	private List<Clock> listAngle = new ArrayList<>();

	public int searchAngle(Clock clock) throws Exception {
		for (Clock c : listAngle) {
			if (c.getHour() == clock.getHour() && c.getMinute() == clock.getMinute()) {
				return c.getAngle();
			}
		}
		return -1;
	}

	public int calculateHourAngle(Clock clock) throws Exception {
		if (clock.getHour() >= 12) {
			clock.setHour(clock.getHour() - 12);
		}
		return clock.getHour() * 30 + (clock.getMinute() / 12) * 6;
	}

	public int calculateMinuteAngle(Clock clock) throws Exception {
		return clock.getMinute() * 6;
	}

	public int compareAngle(int angleHour, int angleMinute) throws Exception {
		int angle = Math.abs(angleHour - angleMinute);

		if (angle > 180) {
			angle -= 360;
		}
		return Math.abs(angle);
	}

	public void addAngle(Clock clock) throws Exception {
		listAngle.add(clock);
	}

}
